-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 17, 2021 at 03:50 PM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 7.2.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `herdikam_dbcat`
--

-- --------------------------------------------------------

--
-- Table structure for table `m_paket_kecermatan`
--

CREATE TABLE `m_paket_kecermatan` (
  `id_paket_kecermatan` int(11) NOT NULL,
  `nama_paket` varchar(25) NOT NULL,
  `id_kecermatan` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_paket_kecermatan`
--

INSERT INTO `m_paket_kecermatan` (`id_paket_kecermatan`, `nama_paket`, `id_kecermatan`) VALUES
(1, 'Kecermatan (Paket 1)', 1),
(2, 'Kecermatan (Paket 2)', 2),
(3, 'Kecermatan (Paket 3)', 0),
(4, 'Kecermatan (Paket 4)', 0),
(5, 'Kecermatan (Paket 5)', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `m_paket_kecermatan`
--
ALTER TABLE `m_paket_kecermatan`
  ADD PRIMARY KEY (`id_paket_kecermatan`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `m_paket_kecermatan`
--
ALTER TABLE `m_paket_kecermatan`
  MODIFY `id_paket_kecermatan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
