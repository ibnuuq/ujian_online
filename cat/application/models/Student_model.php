<?php 

class Student_model extends CI_Model{

	public function student_list()
	{
		return $this->db->select('students.*, added_student.student_id')
			->from('students')
			->join('added_student ','added_student.student_id = students.id','left')
		    ->get()
			->result();
	}

}

?>