<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Students extends CI_Controller 
{
	public function index()
	{
        $this->load->model('student_model');
        $student_list = $this->student_model->student_list();
		$this->load->view('students',['student_list'=>$student_list]);
	}
	
	public function add_selected_student()
{
    $student_id = $this->input->post('student_id'); //here i am getting student id from the checkbox

    for ($i=0; $i < sizeof($student_id); $i++) 
    { 
       $data = array('student_id' => $student_id[$i]);
       $this->db->insert('added_student',$data);
    }
    
    $this->session->set_flashdata('msg',"Students details has been added successfully");
    $this->session->set_flashdata('msg_class','alert-success');

    return redirect('students');
}

}


?>