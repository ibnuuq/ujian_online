<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kecermatan extends CI_Controller {

	public function ikuti_kecermatan($id, $real_id) {

		$pecah_id = substr($id, 6);
		$a['e'] = $this->db->query("SELECT * FROM m_kolom WHERE id_kolom = $real_id")->row_array();
		$a['f'] = $this->db->query("SELECT * FROM m_kecermatan WHERE id_kolom = $real_id")->result_array();
		$a['last_data'] = $this->db->query("SELECT id_kolom FROM m_kolom ORDER BY id_kolom DESC LIMIT 1")->row();
		$a['id_kolom'] = $id;
		$a['real_id'] = $real_id;
		$a['next_real_id'] = $real_id+1;
		$a['id_paket'] = $a['e']['id_pkt_kecermatan'];
		$last_id = $a['e']['nama'];
		$pecah_last = substr($last_id, 6);
		$a['next_kolom'] = $pecah_last+1;
		
		$this->load->view('m_ujian_kecermatan',$a);
	}
	public function submit_ujian_kecermatan()
	{
		date_default_timezone_set('Asia/Jakarta');
		$this->load->model('M_kecermatan');
		$now = date('Y-m-d H:i:s');

		$next_kolom = $this->input->post('next_kolom');
		$id_paket = $this->input->post('id_paket');
		$pecah_next_kolom = substr($next_kolom, 6);

		// var_dump($next_kolom, $id_paket, $pecah_next_kolom);
		// die();

		 $result = array();
		 $jumlah_benar = 0;
	     foreach ($_POST['id_kecermatan'] as $key => $val) {
	         $data = array(             
	            'id_kecermatan' => $val,
	            'id_user' => $_POST['id_user'],
	            'id_kolom'  => $_POST['real_id_kolom'],
	            'jawaban_kecermatan' => $_POST['posradio'.$val][0]
	         );  

	        $where = array('id_kecermatan' => $val , 'id_kolom'  => $_POST['real_id_kolom'] );

			$m_kecermatan = $this->db->where($where)->get('m_kecermatan')->row();

			if($m_kecermatan->jawaban==$_POST['posradio'.$val][0]) {
				$jumlah_benar++;
			}
		
	      	$this->db->insert('tr_kecermatan',$data);   
	     
		}

		$data_tr_ikut_kolom =  array('id_kolom' => $_POST['real_id_kolom'],
									 'id_pkt_kecermatan' => $_POST['id_pkt_kecermatan'],
									  'id_user' => $_POST['id_user'],
									  'jml_benar' => $jumlah_benar,
									  'nilai' => $jumlah_benar );

		
		$this->db->insert('tr_ikut_kecermatan',$data_tr_ikut_kolom); 
		

		if ($pecah_next_kolom > 10) { ?>
			<script>alert('Kecermatan sudah usai!');
			location.href='<?=base_url('adm/ikut_kecermatan/'.$id_paket) ?>'</script>
		<?php
		} else {
			redirect('Kecermatan/ikuti_kecermatan/'.$next_kolom.'/'.$_POST['next_real_id']);
		}
		?>
		<!-- <script>alert('Ke Kolom Selanjutnya!');
			location.href='<?=base_url('adm/ikut_kecermatan') ?>'</script> -->
<?php
}
}

/* End of file Kecermatan.php */
/* Location: ./application/controllers/Kecermatan.php */