<div class="row col-md-12 ini_bodi">
  <div class="panel panel-info">
    <div class="panel-heading">Data Kolom Kecermatan Paket <?=$this->uri->segment(3) ?>
    </div>
    <div class="panel-body">
      <table class="table table-bordered" id="datatabel">
        <thead>
          <tr>
            <th width="5%">No</th>
            <th width="65%">Kecermatan</th>
            <th width="10%">Jumlah Benar</th>
            <th width="10">Nilai</th>
            <th width="10%">Aksi</th>
          </tr>
        </thead>
        <tbody>
          <?php 
          $no =0;
          foreach ($e as $data) {
            $no++;

          ?>
          
          <tr>
            <td><?= $no; ?></td>
            <td><?= $data['nama']; ?></td>
            <td align="center"><?= $data['nilai']; ?></td>
            <td>
               <?php
               $nilai = $data['nilai'];
               if ($nilai <= 10) {
                 echo 44; 
               }elseif ($nilai == 11) {
                 echo "46";
               }elseif ($nilai == 12) {
                 echo 48;
               }
               elseif ($nilai == 13) {
                 echo "50";
               }elseif ($nilai == 14) {
                 echo "52";
               }elseif ($nilai == 15) {
                 echo "54";
               }elseif ($nilai == 16) {
                 echo "56";
               }elseif ($nilai == 17) {
                 echo "58";
               }
               elseif ($nilai == 18) {
                 echo "60";
               }elseif ($nilai == 19 ) {
                 echo "62";
               }elseif ($nilai == 20) {
                 echo "65";
               }elseif ($nilai == 21) {
                 echo "67";
               }elseif ($nilai == 22) {
                 echo "68";
               }elseif ($nilai == 23) {
                 echo "69";
               }elseif ($nilai == 24) {
                 echo "70";
               }elseif ($nilai == 25) {
                 echo "75";
               }elseif ($nilai == 26) {
                 echo "80";
               }elseif ($nilai == 27) {
                 echo "85";
               }elseif ($nilai == 28) {
                 echo "90";
               }elseif ($nilai == 29) {
                 echo "95";
               }else {
                 echo "100";
               }

               ?>
             </td>
            <td><?php if ($data['status_pengerjaan'] == 'Y') { ?>
              <a class="btn btn-success btn-xs"><i class="glyphicon glyphicon-check" style="margin-left: 0px; color: #fff"></i> &nbsp;&nbsp;Sudah dikerjakan</a>
            <?php }else{ ?>
              <a href="<?=base_url('Kecermatan/ikuti_kecermatan/'.$data['nama'].'/'.$data['id_kolom']) ?>" class="btn btn-info btn-xs"><i class="glyphicon glyphicon-pencil" style="margin-left: 0px; color: #fff"></i> &nbsp;&nbsp;Kerjakan</a>
            <?php } ?>
            </td>
            <?php $total += $data['nilai']; 
          } ?>
          </tbody>

      
      </table>
      <div style="float: right;">
        <h2>Skor &nbsp;= <?php echo $total; ?></h2>
      <h2 id="hasil"></h2>  
      </div>
<script>
    var table = document.getElementById("datatabel"), 
    sumHsl = 0;
    for(var t = 1; t < table.rows.length; t++)
    {
        sumHsl = sumHsl + parseInt(table.rows[t].cells[3].innerHTML);
    }
    document.getElementById("hasil").innerHTML = "Hasil = "+ sumHsl/10;
    
</script>
      <canvas id="myChart">
        
      </canvas>
      </div>
    </div>
  </div>
</div>
                    
<div class="modal fade" id="m_guru" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 id="myModalLabel">Data Guru</h4>
      </div>
      <div class="modal-body">
          <form name="f_guru" id="f_guru" onsubmit="return m_guru_s();">
            <input type="hidden" name="id" id="id" value="0">
              <table class="table table-form">
                <tr><td style="width: 25%">NIP</td><td style="width: 75%"><input type="text" class="form-control" name="nip" id="nip" required></td></tr>
                <tr><td style="width: 25%">Nama</td><td style="width: 75%"><input type="text" class="form-control" name="nama" id="nama" required></td></tr>
              </table>
      </div>
      <div class="modal-footer">
        <button class="btn btn-primary"><i class="fa fa-check"></i> Simpan</button>
        <button class="btn" data-dismiss="modal" aria-hidden="true"><i class="fa fa-minus-circle"></i> Tutup</button>
      </div>
        </form>
    </div>
  </div>
</div>
