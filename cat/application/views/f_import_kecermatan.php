<div class="row col-md-12 ini_bodi">
    <div class="panel panel-info">
        <div class="panel-heading">Import Data Kolom
        </div>
        <div class="panel-body">
            <form action="<?php echo base_url(); ?>import/kecermatan"  enctype="multipart/form-data" method="post">
                <input type="hidden" name="id" id="id" value="0">
                <table class="table table-form">
                    <tr><td>File</td><td><input type="file" class="form-control col-md-3" name="import_excel" required></td></tr>
                    <tr><td></td><td>
                        <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> Simpan</button>
                        <a href="<?php echo base_url(); ?>adm/tampil_kolom" class="btn btn-default"><i class="fa fa-minus-circle"></i> Kembali</a>
                    </td></tr>
                </table>
            </form>
        </div>
    </div>
</div>
