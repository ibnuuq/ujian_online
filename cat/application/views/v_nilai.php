<div class="row col-md-12 ini_bodi">
  <div class="panel panel-info">
    <div class="panel-heading">Nilai</div>
    <div class="panel-body">
		<?php foreach ($nilai as $data) { ?>	
        <table class="table table-bordered">
        	<thead>
        		<tr>
        			<th><?=$data['nama'] ?></th>
        			<th>Skor</th>
        		</tr>
        	</thead>
        	<tbody>
        		<tr>
        			<td>Kepribadian</td>
        			<td><?=$data['kepribadian'] ?></td>
        		</tr>
        		<tr>
        			<td>Kecerdasan</td>
        			<td><?=$data['kecerdasan'] ?></td>
        		</tr>
        		<tr>
        			<td>Kecermatan</td>
        			<td><?=$data['kecermatan'] ?></td>
        		</tr>
        		<tr>
        			<td>Total Skor</td>
        			<td><?=($data['kepribadian']*(35/100))+($data['kecerdasan']*(25/100))+($data['kecermatan']*(40/100)) ?></td>
        		</tr>
        	</tbody>
        </table>
		<?php } ?>
        <canvas id="Chartnilai">
        
      </canvas>
      </div>
    </div>
  </div>
</div>
