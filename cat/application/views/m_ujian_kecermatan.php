<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Dashboard - <?php echo $this->config->item('nama_aplikasi')." ".$this->config->item('versi'); ?></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="apple-mobile-web-app-capable" content="yes">
<link href="<?php echo base_url(); ?>___/css/bootstrap.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>___/css/style.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>___/css/buletan_kecermatan.css" rel="stylesheet">
</head>
<body>

<nav class="navbar navbar-findcond navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand"><i class="fa fa-home"></i> <?php echo $this->config->item('nama_aplikasi')." ".$this->config->item('versi'); ?></a>
        </div> 

        <div class="collapse navbar-collapse" id="navbar">
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><?php echo $this->session->userdata('admin_nama')." (".$this->session->userdata('admin_user').")"; ?> <span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="#" onclick="return rubah_password();">Ubah Password</a></li>
                        <li><a href="<?php echo base_url(); ?>adm/logout" onclick="return confirm('keluar..?');">Logout</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>

<div class="container col-md-12" style="margin-top: 70px;">
    <div class="col-md-12">
        <form role="form" name="_form" method="post" id="_form" action="<?= base_url('Kecermatan/submit_ujian_kecermatan') ?>">
            <div class="panel panel-default">
                <div class="panel-heading">Kolom Ke <div class="btn btn-info" id="soalke"><?=substr($e['nama'], 6) ?></div>
                    <div class="tbl-kanan-soal">
                        <div id="timer" style="font-weight: bold" class="btn btn-danger"></div>
                    </div>
                </div>
                <div class="panel-body">
                    
        
        <!-- <div class="card" style="width: 28rem;">
          <img src="<?= base_url('') ?>upload/gambar_kecermatan/gambar_kecermatan_60.jpg" class="card-img-top" alt="..."> -->
          <div class="container">
            <!-- <div class="col-md-12"> -->
          <!-- <center> -->
          <table class="table table-bordered" style="width: 95%;">
            <tr>
              <th colspan="2"><center><?=$e['nama'] ?></center></th>
            </tr>
               
            <tr>
              <td width="3%">A</td>
              <td>
                <?php 
                if ($e['jenis'] == "angka") {
                  echo $e['opsi_a'];
                }else{
                 echo tampil_media_kecermatan('upload/gambar_kecermatan/'.$e[opsi_a]); ?>
                 <?php }?>
               </td>
               
            </tr>
            <tr>
              <td width="3%">B</td>
              <td>
                <?php
                if ($e['jenis'] == "angka") {
                  echo $e['opsi_b'];
                }else{
                 echo tampil_media_kecermatan('upload/gambar_kecermatan/'.$e[opsi_b]) ?>
                 <?php }?>
               </td>
            </tr>
            <tr>
              <td width="3%">C</td>
              <td>
                <?php 
                if ($e['jenis'] == "angka") {
                  echo $e['opsi_c'];
                }else{
                 echo tampil_media_kecermatan('upload/gambar_kecermatan/'.$e[opsi_c]) ?>
                 <?php }?>
               </td>
            </tr>
            <tr>
              <td width="3%">D</td>
              <td>
                <?php
                if ($e['jenis'] == "angka") {
                  echo $e['opsi_d'];
                }else{
                 echo tampil_media_kecermatan('upload/gambar_kecermatan/'.$e[opsi_d]) ?>
                 <?php }?>
               </td>
            </tr>
            <tr>
              <td width="3%">E</td>
              <td>
                <?php
                if ($e['jenis'] == "angka") {
                  echo $e['opsi_e'];
                }else{
                echo tampil_media_kecermatan('upload/gambar_kecermatan/'.$e[opsi_e]) ?>
               <?php }?>
               </td>
            </tr>
            
          </table>
          <!-- </center> -->
          <!-- </div> -->
          </div>
        
      <!-- Content here -->
      
    <div style="overflow: auto;" class="container">
          <table class="table table-bordered" style="overflow: auto; width: 95%;">
            <thead>
              <tr>
                <th width="1%">No</th>
                <th>Soal</th>
                <th>A</th>
                <th>B</th>
                <th>C</th>
                <th>D</th>
                <th>E</th>
              </tr>
            </thead>
          </table>
        </div>
    <div style="height: 250px; overflow: auto;" class="container">
                <table class="table table-hover" style="height: 10px; overflow: auto; margin-left: auto; margin-right: auto;">
          <!-- <thead>
            <tr>
              <th width="5%">No</th>
              <th width="40%">Soal</th>
              <th width="10%">A</th>
              <th width="10%">B</th>
              <th width="10%">C</th>
              <th width="10%">D</th>
              <th width="10%">E</th>
            </tr>
          </thead> -->
         <input type="hidden" name="id_pkt_kecermatan" value="<?=$e['id_pkt_kecermatan'] ?>">
         <input type="hidden" name="id_user" value="<?= $this->session->userdata('admin_id'); ?>">
         <input type="hidden" name="id_kolom" value="<?= $id_kolom ?>">
         <input type="hidden" name="real_id_kolom" value="<?= $real_id ?>">
         <input type="hidden" name="next_real_id" value="<?= $next_real_id ?>">
         <input type="hidden" name="next_kolom" value="<?='kolom-'.$next_kolom ?>">
         <input type="hidden" name="id_paket" value="<?= $id_paket ?>">
            <tbody>
                <?php 
                $no = 0;
                foreach ($f as $key ) {
                    $no++;  
                ?> 
        <tr>
        <th scope="row"><?=$no?></th>
        <td width="20%"><input type="hidden" name="list_soal" value="<?=$key['file'] ?>">
          <?php
          if ($key['jenis'] == '') {
             echo $key['file'];
           }else{
            echo tampil_media_jaw_kecermatan('upload/gambar_kecermatan/'.$key['file']);
            }?>
           </td>
        <td>
          <input type="hidden" name="id_kecermatan[]" value="<?=$key['id_kecermatan'] ?>">
          <div class="radio-inline">
            <label><input type="radio" name="posradio<?= $key['id_kecermatan']; ?>[]" value="A"><span class="checkmark"></span></label>
          </div>
      </td>
      <td>
        <div class="radio-inline">
          <label><input type="radio" name="posradio<?= $key['id_kecermatan']; ?>[]" value="B"><span class="checkmark"></span></label>
        </div>
      </td>
      <td>
        <div class="radio-inline">
          <label><input type="radio" name="posradio<?= $key['id_kecermatan']; ?>[]" value="C"><span class="checkmark"></span></label>
        </div>
      </td>
      <td>
        <div class="radio-inline">
          <label><input type="radio" name="posradio<?= $key['id_kecermatan']; ?>[]" value="D"><span class="checkmark"></span></label>
        </div>
      </td>
      <td>
        <div class="radio-inline">
          <label><input type="radio" name="posradio<?= $key['id_kecermatan']; ?>[]" value="E"><span class="checkmark"></span></label>
        </div>
      </td>
    </tr>
  </tbody>
  
    <?php } ?>

            </div>
            </div>
        </form>
    </div>

   <!--  <div class="col-md-3">
        <div class="panel panel-info">
            <div class="panel-heading">Navigasi Soal</div>
            <div class="panel-body">
                <div id="tampil_jawaban"></div>
            </div>
        </div>
    </div> -->

</div>
          </tbody>
        </table></div>
                <div class="panel-footer">
                    <center><button type="submit" id="submit" class="action submit btn btn-danger btn-lg"><i class="glyphicon glyphicon-stop"></i> Selesai Ujian</button></center>
                    <!-- <input type="hidden" name="jml_soal" value=""> -->
                </div>
            </div>
        </form>
    </div>


</div>
  
<!-- <div class="col-md-12" style="border-top: solid 4px #ddd; text-align: center; padding-top: 10px; margin-top: 50px; margin-bottom: 20px">
    &copy; 2017 <a href="<?php echo base_url(); ?>adm"><?php echo $this->config->item('nama_aplikasi')." ".$this->config->item('versi')."</a> <br> Waktu Server: ".tjs(date('Y-m-d H:i:s'),"s")." - Waktu Database: ".tjs($this->waktu_sql,"s"); ?>. 
</div> -->


<script src="<?php echo base_url(); ?>___/js/jquery-1.11.3.min.js"></script> 
<script src="<?php echo base_url(); ?>___/js/bootstrap.js"></script>
<script src="<?php echo base_url(); ?>___/plugin/countdown/jquery.plugin.min.js"></script> 
<script src="<?php echo base_url(); ?>___/plugin/countdown/jquery.countdown.min.js"></script> 
<script src="<?php echo base_url(); ?>___/plugin/jquery_zoom/jquery.zoom.min.js"></script> 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script>
        var base_url = "<?php echo base_url(); ?>";

        function getFormData($form){
        var unindexed_array = $form.serializeArray();
        var indexed_array = {};
        $.map(unindexed_array, function(n, i){
            indexed_array[n['name']] = n['value'];
        });
        return indexed_array;
    }
            var url = "<?=base_url('Kecermatan/ikuti_kecermatan')?>/<?=$last_id ?>"; // url tujuan
            var count = 120; // dalam detik
            var f_asal  = $("#_form");
            var form  = getFormData(f_asal);
            function countDown() {
                if (count > 0) {
                    count--;
                    var waktu = count + 1;
                    $('#timer').html(waktu + ' detik ' + 'Kolom Selanjutnya');
                    setTimeout("countDown()", 1000);

                } else {
        document.querySelector('#submit').click();
                }
            }
            countDown();
        </script>

</body>
</html>
