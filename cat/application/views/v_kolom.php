<!-- <?php 
$uri4 = $this->uri->segment(4);
?> -->

<div class="row col-md-12 ini_bodi">
  <div class="panel panel-info">
    <div class="panel-heading"><b>Data Soal</b>
      <div class="tombol-kanan">
        <a class="btn btn-success btn-sm" href="<?php echo base_url(); ?>adm/m_soal/edit/0"><i class="glyphicon glyphicon-plus" style="margin-left: 0px; color: #fff"></i> &nbsp;&nbsp;Tambah Data</a>        
        <a class="btn btn-warning btn-sm tombol-kanan" href="<?php echo base_url(); ?>upload/format_soal_download.xlsx" ><i class="glyphicon glyphicon-download"></i> &nbsp;&nbsp;Download Format Import</a>
        <a class="btn btn-info btn-sm tombol-kanan" href="<?php echo base_url(); ?>adm/tampil_import_kolom" ><i class="glyphicon glyphicon-upload"></i> &nbsp;&nbsp;Import Kolom</a>
        <a class="btn btn-info btn-sm tombol-kanan" href="<?php echo base_url(); ?>adm/tampil_import_kecermatan" ><i class="glyphicon glyphicon-upload"></i> &nbsp;&nbsp;Import Kecermatan</a>
        <a href='<?php echo base_url(); ?>adm/m_soal/cetak/<?php echo $uri4; ?>' class='btn btn-info btn-sm' target='_blank'><i class='glyphicon glyphicon-print'></i> Cetak</a>
      </div>
    </div>
    <div class="panel-body">
        
       <!--  <?php echo $this->session->flashdata('k'); ?>
        --> 
        <table class="table table-bordered" id="datatabel">
          <thead>
            <tr>
              <th width="5%">No</th>
              <th width="15%">Paket</th>
              <th width="50%">Nama</th>
              <th width="15%">Aksi</th>
            </tr>
          </thead>

          <tbody>
            <?php
            $no =1;
            foreach ($d as $data) { ?>
            <tr>
              <td><?=$no++ ?></td>
              <td>Kolom Paket <?=$data['id_pkt_kecermatan'] ?></td>
              <td><?=$data['nama'] ?></td>
              <td><center><a href="<?=base_url() ?>" class="btn btn-info btn-xs">Tambah Kecermatan</a></center></td>
            </tr>
            <?php }
            ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
</div>
