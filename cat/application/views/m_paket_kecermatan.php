<div class="row col-md-12 ini_bodi">
  <div class="panel panel-info">
    <div class="panel-heading">Data Kolom
    </div>
    <div class="panel-body">
      <table class="table table-bordered" id="datatabel">
        <thead>
          <tr>
            <th width="5%">No</th>
            <th width="70%">Nama</th>
            <th width="10%">Aksi</th>
          </tr>
        </thead>
        <tbody>
          <?php 
          $no =0;
          foreach ($e as $data) {
            $no++;
          ?>
          
          <tr>
            <td><?= $no; ?></td>
            <td><?= $data['nama_paket']; ?> </td>
            <td>
              <?php foreach ($status as $vs) {
                if ($data['id_paket_kecermatan']==$vs['id_pkt_kecermatan']) {
                  ?>

                  <?php if($vs['status_int']==0){?>
          <a class="btn btn-info btn-xs" href="<?=base_url('adm/ikut_kecermatan/'.$data['id_paket_kecermatan']) ?>">Ikuti Ujian</a>
                  <?php }elseif($vs['status_int']==1){?>
          <a class="btn btn-warning btn-xs" href="<?=base_url('adm/ikut_kecermatan/'.$data['id_paket_kecermatan']) ?>">Lanjut Ujian</a>
                   <?php }elseif($vs['status_int']==2){?>
<a class="btn btn-success btn-xs" >Sudah Mengerjakan</a>
                    <?php }?>


                <?php }
            } ?>
            </td>
          </tr>
        
          <?php 
        } ?>
        </tbody>
      </table>
      <!-- <canvas id="myChart">
        
      </canvas> -->
      </div>
    </div>
  </div>
</div>
                    
<div class="modal fade" id="m_guru" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 id="myModalLabel">Data Guru</h4>
      </div>
      <div class="modal-body">
          <form name="f_guru" id="f_guru" onsubmit="return m_guru_s();">
            <input type="hidden" name="id" id="id" value="0">
              <table class="table table-form">
                <tr><td style="width: 25%">NIP</td><td style="width: 75%"><input type="text" class="form-control" name="nip" id="nip" required></td></tr>
                <tr><td style="width: 25%">Nama</td><td style="width: 75%"><input type="text" class="form-control" name="nama" id="nama" required></td></tr>
              </table>
      </div>
      <div class="modal-footer">
        <button class="btn btn-primary"><i class="fa fa-check"></i> Simpan</button>
        <button class="btn" data-dismiss="modal" aria-hidden="true"><i class="fa fa-minus-circle"></i> Tutup</button>
      </div>
        </form>
    </div>
  </div>
</div>
