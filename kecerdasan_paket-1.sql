-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 20, 2021 at 08:46 AM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 7.2.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_cat`
--

-- --------------------------------------------------------

--
-- Table structure for table `added_student`
--

CREATE TABLE `added_student` (
  `id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `added_student`
--

INSERT INTO `added_student` (`id`, `student_id`) VALUES
(1, 1),
(2, 2),
(3, 3),
(4, 4),
(5, 1),
(6, 5),
(7, 6),
(8, 7),
(9, 8),
(10, 1);

-- --------------------------------------------------------

--
-- Table structure for table `m_admin`
--

CREATE TABLE `m_admin` (
  `id` int(6) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `level` enum('admin','guru','siswa') NOT NULL,
  `kon_id` int(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_admin`
--

INSERT INTO `m_admin` (`id`, `username`, `password`, `level`, `kon_id`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'admin', 0),
(28, '1001', 'b8c37e33defde51cf91e1e03e51657da', 'guru', 2),
(29, '12090677', 'baaf63831059795a0cedec6d705ad519', 'siswa', 7),
(30, '1000', 'a9b7ba70783b617e9998dc4dd82eb3c5', 'guru', 4),
(31, '11090676', '4a4967e9538bf349b057c38a5cffe7d8', 'siswa', 6);

-- --------------------------------------------------------

--
-- Table structure for table `m_guru`
--

CREATE TABLE `m_guru` (
  `id` int(6) NOT NULL,
  `nip` varchar(30) NOT NULL,
  `nama` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_guru`
--

INSERT INTO `m_guru` (`id`, `nip`, `nama`) VALUES
(2, '1001', 'Paket 1'),
(4, '1000', 'Paket 2'),
(5, '1002', 'Paket 3'),
(6, '1003', 'Paket 4'),
(7, '1004', 'Paket 5');

--
-- Triggers `m_guru`
--
DELIMITER $$
CREATE TRIGGER `hapus_guru` AFTER DELETE ON `m_guru` FOR EACH ROW BEGIN
DELETE FROM m_soal WHERE m_soal.id_guru = OLD.id;
DELETE FROM m_admin WHERE m_admin.level = 'guru' AND m_admin.kon_id = OLD.id;
DELETE FROM tr_guru_mapel WHERE tr_guru_mapel.id_guru = OLD.id;
DELETE FROM tr_guru_tes WHERE tr_guru_tes.id_guru = OLD.id;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `m_kecermatan`
--

CREATE TABLE `m_kecermatan` (
  `id_kecermatan` int(11) NOT NULL,
  `id_kolom` int(11) NOT NULL,
  `file` varchar(255) NOT NULL,
  `opsi_a` longtext NOT NULL,
  `opsi_b` longtext NOT NULL,
  `opsi_c` longtext NOT NULL,
  `opsi_d` longtext NOT NULL,
  `opsi_e` longtext NOT NULL,
  `jawaban` varchar(5) NOT NULL,
  `tgl_input` datetime NOT NULL,
  `jml_benar` int(11) NOT NULL,
  `jml_salah` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `m_kolom`
--

CREATE TABLE `m_kolom` (
  `id_kolom` int(11) NOT NULL,
  `nama` varchar(25) NOT NULL,
  `opsi_a` int(11) NOT NULL,
  `opsi_b` int(11) NOT NULL,
  `opsi_c` int(11) NOT NULL,
  `opsi_d` int(11) NOT NULL,
  `opsi_e` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `m_mapel`
--

CREATE TABLE `m_mapel` (
  `id` int(6) NOT NULL,
  `nama` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_mapel`
--

INSERT INTO `m_mapel` (`id`, `nama`) VALUES
(1, 'Kecerdasan'),
(2, 'Kepribadian'),
(3, 'Matematika'),
(4, 'IPA');

--
-- Triggers `m_mapel`
--
DELIMITER $$
CREATE TRIGGER `hapus_mapel` AFTER DELETE ON `m_mapel` FOR EACH ROW BEGIN
DELETE FROM m_soal WHERE m_soal.id_mapel = OLD.id;
DELETE FROM tr_guru_mapel WHERE tr_guru_mapel.id_mapel = OLD.id;
DELETE FROM tr_guru_tes WHERE tr_guru_tes.id_mapel = OLD.id;
DELETE FROM tr_siswa_mapel WHERE tr_siswa_mapel.id_mapel = OLD.id;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `m_siswa`
--

CREATE TABLE `m_siswa` (
  `id` int(6) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `nim` varchar(50) NOT NULL,
  `jurusan` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_siswa`
--

INSERT INTO `m_siswa` (`id`, `nama`, `nim`, `jurusan`) VALUES
(1, 'Agus Yudhoyono', '12090671', 'Teknik Informatika'),
(2, 'Edi Baskoro Yudhoyono', '12090672', 'Teknik Informatika'),
(3, 'Puan Maharani', '11090673', 'Sistem Informasi'),
(4, 'Kaesang Pangarep', '11090674', 'Sistem Informasi'),
(5, 'Anisa Pohan', '12090675', 'Teknik Informatika'),
(6, 'Gibran Rakabuming Raka', '11090676', 'Sistem Informasi'),
(7, 'Kahiyang Ayu', '12090677', 'Teknik Informatika');

--
-- Triggers `m_siswa`
--
DELIMITER $$
CREATE TRIGGER `hapus_siswa` AFTER DELETE ON `m_siswa` FOR EACH ROW BEGIN
DELETE FROM tr_ikut_ujian WHERE tr_ikut_ujian.id_user = OLD.id;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `m_soal`
--

CREATE TABLE `m_soal` (
  `id` int(6) NOT NULL,
  `id_guru` int(6) NOT NULL,
  `id_mapel` int(6) NOT NULL,
  `bobot` int(2) NOT NULL,
  `file` varchar(150) NOT NULL,
  `tipe_file` varchar(50) NOT NULL,
  `soal` longtext NOT NULL,
  `opsi_a` longtext NOT NULL,
  `opsi_b` longtext NOT NULL,
  `opsi_c` longtext NOT NULL,
  `opsi_d` longtext NOT NULL,
  `opsi_e` longtext NOT NULL,
  `jawaban` varchar(5) NOT NULL,
  `tgl_input` datetime NOT NULL,
  `jml_benar` int(6) NOT NULL,
  `jml_salah` int(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_soal`
--

INSERT INTO `m_soal` (`id`, `id_guru`, `id_mapel`, `bobot`, `file`, `tipe_file`, `soal`, `opsi_a`, `opsi_b`, `opsi_c`, `opsi_d`, `opsi_e`, `jawaban`, `tgl_input`, `jml_benar`, `jml_salah`) VALUES
(325, 2, 1, 1, '', '', 'generik x ….', '#####obat ', '#####murah ', '#####umum', '#####spesifik', '#####jenis', 'D', '2021-03-20 12:55:22', 0, 0),
(326, 2, 1, 1, '', '', 'asimilasi x…', '#####perselarasan ', '#####harmoni', '#####kebangkitan', '#####tidak setuju', '#####pertengkarang', 'E', '2021-03-20 12:55:22', 0, 0),
(327, 2, 1, 1, '', '', 'relatif x…', '#####biasa', '#####ukuran ', '#####pandangan', '#####pasti ', '#####tergantung', 'D', '2021-03-20 12:55:22', 0, 0),
(328, 2, 1, 1, '', '', 'beringin : pohon = … : … ', '#####biologi : laboratorium', '#####geologi : ilmu', '#####astronomi : galaksi', '#####kimia : senyawa ', '#####teori : praktik', 'B', '2021-03-20 12:55:22', 0, 0),
(329, 2, 1, 1, '', '', 'dangdut : musik', '#####buku : baca', '#####horor :film', '#####biografi : autobiografi', '#####padi:beras', '#####muay thai:suku', 'B', '2021-03-20 12:55:22', 0, 0),
(330, 2, 1, 1, '', '', 'nelayan:perahu = fotografer :', '#####kamera', '#####model', '#####seni', '#####rol film ', '#####lensa', 'A', '2021-03-20 12:55:22', 0, 0),
(331, 2, 1, 1, 'gambar_soal_331.jpg', 'image/jpeg', '<p>Soal No 7</p>\r\n', 'gja_331.jpg#####<p>A</p>\r\n', 'gjb_331.jpg#####<p>B</p>\r\n', 'gjc_331.jpg#####<p>C</p>\r\n', 'gjd_331.jpg#####<p>D</p>\r\n', 'gje_331.jpg#####<p>E</p>\r\n', 'A', '2021-03-20 12:55:22', 0, 0),
(332, 2, 1, 1, 'gambar_soal_332.jpg', 'image/jpeg', '<p>Soal No 8</p>\r\n', 'gja_332.jpg#####<p>A</p>\r\n', 'gjb_332.jpg#####<p>B</p>\r\n', 'gjc_332.jpg#####<p>C</p>\r\n', 'gjd_332.jpg#####<p>D</p>\r\n', 'gje_332.jpg#####<p>E</p>\r\n', 'C', '2021-03-20 12:55:22', 0, 0),
(333, 2, 1, 1, 'gambar_soal_333.jpg', 'image/jpeg', '<p>Soal No. 9</p>\r\n', 'gja_333.jpg#####<p>A</p>\r\n', 'gjb_333.jpg#####<p>B</p>\r\n', 'gjc_333.jpg#####<p>C</p>\r\n', 'gjd_333.jpg#####<p>D</p>\r\n', 'gje_333.jpg#####<p>E</p>\r\n', 'C', '2021-03-20 12:55:22', 0, 0),
(334, 2, 1, 1, 'gambar_soal_334.jpg', 'image/jpeg', '<p>Soal No. 10</p>\r\n', '#####<p>2-4-1-3</p>\r\n', '#####<p>3-2-4-1</p>\r\n', '#####<p>1-2-4-3</p>\r\n', '#####<p>4-1-2-3</p>\r\n', '#####<p>4-2-1-3</p>\r\n', 'E', '2021-03-20 12:55:22', 0, 0),
(335, 2, 1, 1, 'gambar_soal_335.jpg', 'image/jpeg', '<p>gambar soal 11</p>\r\n', '#####<p>1-3-2-4</p>\r\n', '#####<p>3-1-4-2</p>\r\n', '#####<p>4-2-1-3</p>\r\n', '#####<p>1-2-3-4</p>\r\n', '#####<p>4-1-2-3</p>\r\n', 'A', '2021-03-20 12:55:22', 0, 0),
(336, 2, 1, 1, 'gambar_soal_336.jpg', 'image/jpeg', '<p>gambar soal 12</p>\r\n', '#####<p>3-4-2-1</p>\r\n', '#####<p>1-2-3-4</p>\r\n', '#####<p>3-1-4-2</p>\r\n', '#####<p>1-2-4-2</p>\r\n', '#####<p>3-1-2-4</p>\r\n', 'C', '2021-03-20 12:55:22', 0, 0),
(337, 2, 1, 1, '', '', 'B   F  E  I  H ….   ….', '#####KL', '#####IK', '#####JK', '#####LK', '#####L J', 'D', '2021-03-20 12:55:22', 0, 0),
(338, 2, 1, 1, '', '', 'HX  HW  HT  …   ...', '#####HO,HM', '#####HR,HO', '#####HS,HP', '#####HL,HN', '#####HQ,HN', 'C', '2021-03-20 12:55:22', 0, 0),
(339, 2, 1, 1, '', '', 'ACB  FHG  KML …   …. ', '#####NBC,WUT', '#####ONO,RST', '#####PRQ,UWV', '#####SUT,XWV', '#####PRQ.UWV', 'C', '2021-03-20 12:55:22', 0, 0),
(340, 2, 1, 1, '', '', '25x8+16x5-24:6', '#####277', '#####168', '#####226', '#####276', '#####176', 'D', '2021-03-20 12:55:22', 0, 0),
(341, 2, 1, 1, '', '', ' ( 0,21 ) pangkat 2', '#####0.0414', '#####0.0661', '#####0.551', '#####0.0841', '#####0.0441', 'E', '2021-03-20 12:55:22', 0, 0),
(342, 2, 1, 1, '', '', '45:5+136:47', '#####101', '#####111', '#####121', '#####120', '#####130', 'D', '2021-03-20 12:55:22', 0, 0),
(343, 2, 1, 1, '', '', 'paras = ', '#####serasi', '#####aura', '#####wajah', '#####bertemu ', '#####penampilan', 'C', '2021-03-20 12:55:22', 0, 0),
(344, 2, 1, 1, '', '', 'korelasi = ', '#####timbangan', '#####konspirasi', '#####berlawanan', '#####hubungan ', '#####sindikan', 'D', '2021-03-20 12:55:22', 0, 0),
(345, 2, 1, 1, '', '', 'virtual = ', '#####nyata', '#####impian', '##### keadaan', '#####hipotema', '#####maya', 'A', '2021-03-20 12:55:22', 0, 0),
(346, 2, 1, 1, '', '', 'Jika tidak makan maka tubuh akan lemas. Jika tubuh !emas maka mudah sakit.', '#####Jika sakit maka tubuh temas', '#####Jika sakit maka tubuh temas', '#####Jika tidak makan maka tidak mudah sakit', '#####Jika makan maka tubuh akan sakit.', '#####Jika tidak makan maka mudah sakit.', 'E', '2021-03-20 12:55:22', 0, 0),
(347, 2, 1, 1, '', '', 'Semua kopi murni terasa pahit dan berwarna hitam. Kopi yang dibuat Reza rasanya manis.\n     Kesimpulan yang paling tepat pada kedua kalimat diatas adalah ', '#####Kopi yang dibuat Reza sudah ditambah gula.', '#####Kopi yang dibuat Reza tidak herwarna hitam.', '#####Kopi yang dibuat Reza bukn kopi murni.', '#####Kopi yang dibuat Reza tetap berwarna hitarn.', '#####Kopi yang dibuat Reza sudah ditambah susu.', 'C', '2021-03-20 12:55:22', 0, 0),
(348, 2, 1, 1, '', '', 'Sebagian A adalah B. Semua A adalah C. keseimpulan yg paling tepat pada kalimat tersebut adalahh…', '#####Semua A yang bukan C adalah B.', '#####Sebagan C adalah B.', '#####Sebagian B adalah A.', '#####Semua B yang bukan A adalah C.', '#####Tidak dapa? diambil kesimpulan.', 'B', '2021-03-20 12:55:22', 0, 0),
(349, 2, 1, 1, 'gambar_soal_349.jpg', 'image/jpeg', '<p>gambar no 25</p>\r\n', '#####<p>12</p>\r\n', '#####<p>13</p>\r\n', '#####<p>14</p>\r\n', '#####<p>15</p>\r\n', '#####<p>16</p>\r\n', 'D', '2021-03-20 12:55:22', 0, 0),
(350, 2, 1, 1, 'gambar_soal_350.jpg', 'image/jpeg', '<p>gambar no 26</p>\r\n', '#####<p>15</p>\r\n', '#####<p>18</p>\r\n', '#####<p>10</p>\r\n', '#####<p>12</p>\r\n', '#####<p>14</p>\r\n', 'E', '2021-03-20 12:55:22', 0, 0),
(351, 2, 1, 1, 'gambar_soal_351.jpg', 'image/jpeg', '<p>gambar no 27</p>\r\n', '#####<p>7</p>\r\n', '#####<p>10</p>\r\n', '#####<p>13</p>\r\n', '#####<p>12</p>\r\n', '#####<p>14</p>\r\n', 'B', '2021-03-20 12:55:22', 0, 0),
(352, 2, 1, 1, 'gambar_soal_352.jpg', 'image/jpeg', '<p>gambar no28</p>\r\n', 'gja_352.jpg#####<p>A</p>\r\n', 'gjb_352.jpg#####<p>B</p>\r\n', 'gjc_352.jpg#####<p>C</p>\r\n', 'gjd_352.jpg#####<p>D</p>\r\n', 'gje_352.jpg#####<p>E</p>\r\n', 'B', '2021-03-20 12:55:22', 0, 0),
(353, 2, 1, 1, 'gambar_soal_353.jpg', 'image/jpeg', '<p>gambar no 29</p>\r\n', 'gja_353.jpg#####<p>A</p>\r\n', 'gjb_353.jpg#####<p>B</p>\r\n', 'gjc_353.jpg#####<p>C</p>\r\n', 'gjd_353.jpg#####<p>..</p>\r\n', 'gje_353.jpg#####<p>..</p>\r\n', 'A', '2021-03-20 12:55:22', 0, 0),
(354, 2, 1, 1, 'gambar_soal_354.jpg', 'image/jpeg', '<p>gambar no 30</p>\r\n', 'gja_354.jpg#####<p>..</p>\r\n', 'gjb_354.jpg#####<p>B</p>\r\n', 'gjc_354.jpg#####<p>C</p>\r\n', 'gjd_354.jpg#####<p>D</p>\r\n', 'gje_354.jpg#####<p>E</p>\r\n', 'B', '2021-03-20 12:55:22', 0, 0),
(355, 2, 1, 1, '', '', '100 4 90 7 80', '#####8', '#####9', '#####10', '#####11', '#####12', 'C', '2021-03-20 12:55:22', 0, 0),
(356, 2, 1, 1, '', '', '7 6 14 12 22 18 31 …. …. ', '#####24, 41', '#####32, 19', '#####25.4', '#####23, 40', '#####21, 41', 'A', '2021-03-20 12:55:22', 0, 0),
(357, 2, 1, 1, '', '', '4 8 16   ….  ….   …..    2 4 8 1 2 4', '#####3, 7, 13', '#####5, 9, 17', '#####5, 7, 13', '#####4, 6, 12 ', '#####3, 6, 12', 'E', '2021-03-20 12:55:22', 0, 0),
(358, 2, 1, 1, '', '', 'Shintg bhir pada tanggal 17 Mei 1991, dia merupakan salah satu mahasiswi di perguruan tinggi negeri jakarta. Jika hari ini adalah tanggal 2 april, berpa usia shinta minggu depan', '#####25 Tahun 11 Bulan 28 Hari', '#####26 Tahun 10 Bulan 31 Hari', '#####26 Tahun 11 Bulan 3 Hari ', '#####25 Tahun 10 Buian 32 Hari', '#####24 Tahun 11 Bulan 10 Hari', 'A', '2021-03-20 12:55:22', 0, 0),
(359, 2, 1, 1, '', '', ' Untuk mengunduh sebuah video berkapasitas 2 Gb komputer membutuhkan waktu', '#####13.22 WIB', '#####15.22 WIB     ', '#####14.23 W1B', '#####14.24 W1B    ', '#####14.22 WIB', 'E', '2021-03-20 12:55:22', 0, 0),
(360, 2, 1, 1, '', '', 'Tarif parkir mobil di sebuah mall sebesar Rp. 2.000,-/jam untuk 2 iam pertama, selanjutnya Rp.1.500,-/jam. Jika Mutia masuk kedaiam mall tersebut pada pukul 13.40 \nWIB untuk membeli keperluan rumah tangga selama 2 jam, kemudian Mutia makan 30 \nmenit lalu ke salon selama 1 jam 20 menit dan terakhir Mutia nonton bioskop dengan  durasi film 120 rnenit. Mutia turun dari gedung bioskop sampai ke parkiran menghabiskan waktu 10 menit, maka berapa kira-kira tarif parkir yang harus dibayar  oleh Mutia?', '#####Rp. 11.000,-', '#####Rp. 9.500,-     ', '#####Rp. 13.000,-', '#####Rp. 10.000,- ', '#####Rp. 10.500,-', 'D', '2021-03-20 12:55:22', 0, 0),
(361, 2, 1, 1, '', '', 'Mendung : Malam = …. : …..', '#####Hujan - Senja  ', '#####Awan - Bulan ', '#####Ke!abu - Gelap   ', '#####Matahari - Bintang', '#####Cuaca - Waktu', 'A', '2021-03-20 12:55:22', 0, 0),
(362, 2, 1, 1, '', '', '…. :Burung = Roda : ….', '#####Sangkar - Motor ', '#####Terbang - Jalan raya', '#####Sayap - Mobil ', '#####Cakar - Stir ', '#####Cakar - Stir ', 'C', '2021-03-20 12:55:22', 0, 0),
(363, 2, 1, 1, '', '', 'Penyuntingan : ….. =      - -          : Pemilihan', '#####Percetakan - Perwakilan ', '#####Penerbitan - Kampanye   ', '#####Penutis - Presiden', '#####Perwakilan - Pencoblosan', '#####Penyunting - Rakyat', 'B', '2021-03-20 12:55:22', 0, 0),
(364, 2, 1, 1, '', '', 'Lepas landas X ', '#####Berangkat ', '#####Datang ', '#####Hinggap ', '##### Terbang   ', '#####turun', 'B', '2021-03-20 12:55:22', 0, 0),
(365, 2, 1, 1, '', '', 'Abadi X ', '#####Sesaat   ', '##### Kekal     ', '#####Tidak berujung ', '#####Selamanya       ', '#####Hidup', 'A', '2021-03-20 12:55:22', 0, 0),
(366, 2, 1, 1, '', '', ' Awam X ', '#####Hujan  ', '#####Pakar ', '#####umum', '#####Guru ', '#####Terbiasa', 'B', '2021-03-20 12:55:22', 0, 0),
(367, 2, 1, 1, 'gambar_soal_367.jpg', 'image/jpeg', '<p>gambar no 43</p>\r\n', 'gja_367.jpg#####<p>A</p>\r\n', 'gjb_367.jpg#####<p>B</p>\r\n', 'gjc_367.jpg#####<p>C</p>\r\n', 'gjd_367.jpg#####<p>D</p>\r\n', 'gje_367.jpg#####<p>E</p>\r\n', 'D', '2021-03-20 12:55:22', 0, 0),
(368, 2, 1, 1, 'gambar_soal_368.jpg', 'image/jpeg', '<p>gambar no 44</p>\r\n', 'gja_368.jpg#####<p>A</p>\r\n', 'gjb_368.jpg#####<p>B</p>\r\n', 'gjc_368.jpg#####<p>C</p>\r\n', 'gjd_368.jpg#####<p>D</p>\r\n', 'gje_368.jpg#####<p>E</p>\r\n', 'D', '2021-03-20 12:55:22', 0, 0),
(369, 2, 1, 1, 'gambar_soal_369.jpg', 'image/jpeg', '<p>gambar no&nbsp;45</p>\r\n', 'gja_369.jpg#####<p>A</p>\r\n', 'gjb_369.jpg#####<p>B</p>\r\n', 'gjc_369.jpg#####<p>C</p>\r\n', 'gjd_369.jpg#####<p>D</p>\r\n', 'gje_369.jpg#####<p>E</p>\r\n', 'D', '2021-03-20 12:55:22', 0, 0),
(370, 2, 1, 1, 'gambar_soal_370.jpg', 'image/jpeg', '<p>gambar no 46</p>\r\n', 'gja_370.jpg#####<p>A</p>\r\n', 'gjb_370.jpg#####<p>B</p>\r\n', 'gjc_370.jpg#####<p>C</p>\r\n', 'gjd_370.jpg#####<p>D</p>\r\n', 'gje_370.jpg#####<p>E</p>\r\n', 'D', '2021-03-20 12:55:22', 0, 0),
(371, 2, 1, 1, 'gambar_soal_371.jpg', 'image/jpeg', '<p>gambar no 47</p>\r\n', 'gja_371.jpg#####<p>A</p>\r\n', 'gjb_371.jpg#####<p>B</p>\r\n', 'gjc_371.jpg#####<p>C</p>\r\n', 'gjd_371.jpg#####<p>D</p>\r\n', 'gje_371.jpg#####<p>E</p>\r\n', 'E', '2021-03-20 12:55:22', 0, 0),
(372, 2, 1, 1, 'gambar_soal_372.jpg', 'image/jpeg', '<p>Gambar no 48</p>\r\n', 'gja_372.jpg#####<p>A</p>\r\n', 'gjb_372.jpg#####<p>B</p>\r\n', 'gjc_372.jpg#####<p>C</p>\r\n', 'gjd_372.jpg#####<p>D</p>\r\n', 'gje_372.jpg#####<p>E</p>\r\n', 'A', '2021-03-20 12:55:22', 0, 0),
(373, 2, 1, 1, '', '', 'Dari 40 orang anggola Karang Taruna, 21 orang gemar olahraga tenis remaja, 27 orang gernar olahraga bulu tangkis, dan 15 orang gemar kedua ofahnaga balk tenis - \n          maupun bulu tangkis Banyak anggota Karang Taruna yang tidak gemar tenis \n          rnaupun bulu tangkis adalah', '#####8 orang', '#####25', '#####17', '#####18', '#####7', 'E', '2021-03-20 12:55:22', 0, 0),
(374, 2, 1, 1, '', '', 'Ariji marnpu memeriksa mobil dalam waklu 20  menit. Sementara riko hanya butuh 18 menit.  Mereka rnulai belketrja pada jam 09 00 WIB, pada jarn berapa kira kira mereka \n           pertama kali bersamaan menyelesaikan pemeriksaan mobli?', '#####11.00', '#####09 30', '#####12.00', '#####13.00', '#####10 00', 'C', '2021-03-20 12:55:22', 0, 0),
(375, 2, 1, 1, '', '', 'Seorang pedagang beras membeli beras tipe A sebanyak 20 kg dengan harga Rp 6000 / kg, dan beras tipe sebanyak 30 kg dengan harga Rp 4000 / kg, kernudian \n            ber4s tersebut dicampur. Apabila pedagang tersebut ingin menjual kembali beras\n            yang sudah clicampur tersebut dengan untung 4%, maka berapa harga per kilo beras \n            yang harus dijual?', '#####Rp. 4.500', '#####Rp. 6.700 ', '#####rp .5300', '#####Rp. 4.750 ', '#####Rp. 4.992', 'E', '2021-03-20 12:55:22', 0, 0),
(376, 2, 1, 1, '', '', 'FX IW LT 00 ….', '#####ST ', '#####rt', '#####pq', '#####rh', '#####wj', 'D', '2021-03-20 12:55:22', 0, 0),
(377, 2, 1, 1, '', '', ' BWO GSE LOU QKI', '##### WGB', '#####VHC', '#####VGA', '#####PJA ', '#####RGA', 'C', '2021-03-20 12:55:22', 0, 0),
(378, 2, 1, 1, '', '', 'ZRK   XQL   VOM   SLN   QKO  …..   …..', '#####MJP   QFK', '#####NJO    LGR ', '#####OHP    MGQ', '#####NPL   LGJ', '#####OIR    JGR', 'C', '2021-03-20 12:55:22', 0, 0),
(379, 2, 1, 1, '', '', '\n  (Bdcaan untuk nomor. 55 sld 58) Di sebuah Rumah sedang beriangsurg acara arisan keluarga Ada 13 orang yang hadIR pada\n  cara tersebut. Hubungan diantata mereka sebagat berskut                                                                        r..`4\n          •   J dan K adalah cucu dari G.                                                                                            \n           •  C adalah nenek dari H, A, dan F\n           •  D dan M adalah menantu perempuan dari G                                                                      \n           •  G adalah suamii dari C.\n           •  B adalah ibu dari H dan istri dati L.\n           •  M adalah istri dari E dan ibu dan J dan A\n           •   K adalah adik perempuan F dan putta D  Siapakah kakek dari A?', '#####I', '#####E', '#####L', '#####G', '#####M', 'D', '2021-03-20 12:55:22', 0, 0),
(380, 2, 1, 1, '', '', ' (Bdcaan untuk nomor. 55 sld 58) Di sebuah Rumah sedang beriangsurg acara arisan keluarga Ada 13 orang yang hadIR pada\n  cara tersebut. Hubungan diantata mereka sebagat berskut                                                                        r..`4\n          •   J dan K adalah cucu dari G.                                                                                            \n           •  C adalah nenek dari H, A, dan F\n           •  D dan M adalah menantu perempuan dari G                                                                      \n           •  G adalah suamii dari C.\n           •  B adalah ibu dari H dan istri dati L.\n           •  M adalah istri dari E dan ibu dan J dan A\n           •   K adalah adik perempuan F dan putta D   ........Jika I adalah suami D, manakah dari pernyataan bberikut yg benar ?', '#####I adalah anak C', '#####E ada;ah sepupu I', '#####H adalah Sepupu I', '#####F anak kandung dari I', '#####D saudara kandung E', 'A', '2021-03-20 12:55:22', 0, 0),
(381, 2, 1, 1, '', '', '(Bdcaan untuk nomor. 55 sld 58) Di sebuah Rumah sedang beriangsurg acara arisan keluarga Ada 13 orang yang hadIR pada\n  cara tersebut. Hubungan diantata mereka sebagat berskut                                                                        r..`4\n          •   J dan K adalah cucu dari G.                                                                                            \n           •  C adalah nenek dari H, A, dan F\n           •  D dan M adalah menantu perempuan dari G                                                                      \n           •  G adalah suamii dari C.\n           •  B adalah ibu dari H dan istri dati L.\n           •  M adalah istri dari E dan ibu dan J dan A\n           •   K adalah adik perempuan F dan putta D  ...................Siap2 saja saudara sepupu dari F?', '#####A, J dan K.', '#####A, H dan J', '#####H„J dan K.', '##### H, I dan J', '#####A, B dan K.', 'B', '2021-03-20 12:55:22', 0, 0),
(382, 2, 1, 1, '', '', 'Inspeksi =', '#####Inveksi', '#####Terjangkit', '#####Pemeriksaan', '#####Pengulangan', '#####Observasi', 'C', '2021-03-20 12:55:22', 0, 0),
(383, 2, 1, 1, '', '', 'Konfiden =', '#####Gejala ', '#####Ragu ', '#####CEKUNG', '#####jujur', '#####yakin', 'E', '2021-03-20 12:55:22', 0, 0),
(384, 2, 1, 1, '', '', 'Hedonisme =', '#####Hura - hura', '#####Hemat ', '#####aktif', '#####Jiwa pahlawan', '#####Besar dan megah', 'A', '2021-03-20 12:55:22', 0, 0),
(385, 2, 1, 1, 'gambar_soal_385.jpg', 'image/jpeg', '<p>gambar no 61</p>\r\n', '#####<p>14</p>\r\n', '#####<p>20</p>\r\n', '#####<p>21</p>\r\n', '#####<p>19</p>\r\n', '#####<p>17</p>\r\n', 'C', '2021-03-20 12:55:22', 0, 0),
(386, 2, 1, 1, 'gambar_soal_386.jpg', 'image/jpeg', '<p>gambar no 62</p>\r\n', '#####<p>27</p>\r\n', '#####<p>15</p>\r\n', '#####<p>25</p>\r\n', '#####<p>23</p>\r\n', '#####<p>33</p>\r\n', 'A', '2021-03-20 12:55:22', 0, 0),
(387, 2, 1, 1, 'gambar_soal_387.jpg', 'image/jpeg', '<p>gambar no 63</p>\r\n', '#####<p>15</p>\r\n', '#####<p>10</p>\r\n', '#####<p>13</p>\r\n', '#####<p>18</p>\r\n', '#####<p>20</p>\r\n', 'D', '2021-03-20 12:55:22', 0, 0),
(388, 2, 1, 1, 'gambar_soal_388.jpg', 'image/jpeg', '<p>gambar no 64</p>\r\n', '#####<p>2 &mdash; 1 &mdash; 4 &mdash; 3</p>\r\n', '#####<p>3 &mdash; 4 &mdash; 1 &mdash;&nbsp;2</p>\r\n', '#####<p>2 &mdash; 1 &mdash;3 &mdash; 4</p>\r\n', '#####<p>3&nbsp;&mdash; 4 &mdash; 2&nbsp;&mdash; 1</p>\r\n', '#####<p>4 &mdash; 1 &mdash; 3&nbsp;&mdash; 2</p>\r\n', 'C', '2021-03-20 12:55:22', 0, 0),
(389, 2, 1, 1, 'gambar_soal_389.jpg', 'image/jpeg', '<p>gambar no 65</p>\r\n', '#####<p>2&nbsp;&mdash; 3&nbsp;&mdash; 1 &mdash;&nbsp;4</p>\r\n', '#####<p>3 &mdash; 4&nbsp;&mdash; 1&nbsp;&mdash; 2</p>\r\n', '#####<p>1 &mdash; 2&nbsp;&mdash; 3&nbsp;&mdash; 4</p>\r\n', '#####<p>3 &mdash; 1&nbsp;&mdash; 4&nbsp;&mdash; 2</p>\r\n', '#####<p>2 &mdash; 1 &mdash; 3&nbsp;&mdash; 4</p>\r\n', 'D', '2021-03-20 12:55:22', 0, 0),
(390, 2, 1, 1, 'gambar_soal_390.jpg', 'image/jpeg', '<p>gambar no 66</p>\r\n', '#####<p>1-3-4-2-5</p>\r\n', '#####<p>3 -4 - 1- 2 -5</p>\r\n', '#####<p>2-4-5-1-3</p>\r\n', '#####<p>3 -2-4- 1 -5</p>\r\n', '#####<p>3-2 -4- 5 -1</p>\r\n', 'E', '2021-03-20 12:55:22', 0, 0),
(391, 2, 1, 1, '', '', '…...+ 5/7 = 47 / 56', '#####2/8..', '#####44411', '#####.7/9', '#####.1/8  ', '#####. 9 / 7', 'D', '2021-03-20 12:55:22', 0, 0),
(392, 2, 1, 1, '', '', '( 2 x 2)2 + 5 x 6 + 4 + 52 + 5 = ', '#####90', '#####145', '#####70', '#####160', '#####80', 'E', '2021-03-20 12:55:22', 0, 0),
(393, 2, 1, 1, '', '', '2 / 6 : (2/3 : 1 / 2) =', '#####. 3 / 5   ', '#####. 2 / 7  ', '#####. 1 / 4', '#####. 2 / 6    ', '#####. 1 / 6', 'C', '2021-03-20 12:55:22', 0, 0),
(394, 2, 1, 1, '', '', ' 226    223   218    211  202 …..  ……', '#####178  190', '##### 191  178 ', '#####191  179', '#####191  190  ', '##### 138  178', 'B', '2021-03-20 12:55:22', 0, 0),
(395, 2, 1, 1, '', '', '37   74   73   219    217 …. .  …..', '#####867 864   ', '#####868 865 ', '#####. 869 856', '#####868 864    ', '#####865 856', 'B', '2021-03-20 12:55:22', 0, 0),
(396, 2, 1, 1, '', '', '68   34    36    18    21     10,5   …..   …..', '#####14,5     7,25 ', '##### 11     5,5', '#####15     7,5', '#####13,5    7,5', '##### 13     6,5', 'A', '2021-03-20 12:55:22', 0, 0),
(397, 2, 1, 1, '', '', 'Konduktor X ', '#####Penghantar ', '#####Penyumbat ', '#####Masinis', '#####Pilot ', '#####Penghambat', 'E', '2021-03-20 12:55:22', 0, 0),
(398, 2, 1, 1, '', '', 'Mutualisme X ', '#####Amutualisme  ', '#####Simblosisme ', '#####Ekosistem', '#####Parasitisme', '#####Parasitisme', 'D', '2021-03-20 12:55:22', 0, 0),
(399, 2, 1, 1, '', '', 'imigrasi X', '##### Migrusi ', '#####Emigrasi', '#####Transmigrasi', '#####Pemukiman', '#####Kampung', 'B', '2021-03-20 12:55:22', 0, 0),
(400, 2, 1, 1, '', '', ' Jaian Raya : ….     = …...     :  Guling', '#####pol lantas -selimut', '#####rambu lalu lintas- tenaga', '#####kecelakaan -  tidur', '#####keamanan lalu lintas - elektronik', '#####lampu lalu lintas - kasur', 'E', '2021-03-20 12:55:22', 0, 0),
(401, 2, 1, 1, '', '', 'Oven : .„ .... . -= ….   :     Baterai', '#####Isanas - Zat kimia  ', '#####Panggang Tenaga', '#####Listrik - Kamera', '#####Pengukur suhu - Etektronik', '##### Matang - Radio', 'C', '2021-03-20 12:55:22', 0, 0),
(402, 2, 1, 1, '', '', ' Tropis :  …...      = …...      :    Pohon', '#####Cuaca - Hutan', '#####ikiim -  Cemara', '#####Sinar matahari -  Pegunungan', '#####Panas - Tumbuhan  ', '#####Khatulistiwa - Hijau', 'B', '2021-03-20 12:55:22', 0, 0),
(403, 2, 1, 1, '', '', '   ….  : Perdamaian = Perkawinan  :  …..', '#####Persahabatan - Pasangan ', '#####Kesetiaan - Pertunangan', '#####Persatuan - Keluarga', '#####Permusuhan - Perceraian ', '#####Perkumpulan - Pergaulan', 'D', '2021-03-20 12:55:22', 0, 0),
(404, 2, 1, 1, 'gambar_soal_404.jpg', 'image/jpeg', '<p>gambar no 80</p>\r\n', '#####<p>11</p>\r\n', '#####<p>12</p>\r\n', '#####<p>13</p>\r\n', '#####<p>14</p>\r\n', '#####<p>15</p>\r\n', 'E', '2021-03-20 12:55:22', 0, 0),
(405, 2, 1, 1, 'gambar_soal_405.jpg', 'image/jpeg', '<p>gambar no 81</p>\r\n', '#####<p>20</p>\r\n', '#####<p>21</p>\r\n', '#####<p>22</p>\r\n', '#####<p>23</p>\r\n', '#####<p>24</p>\r\n', 'E', '2021-03-20 12:55:22', 0, 0),
(406, 2, 1, 1, 'gambar_soal_406.jpg', 'image/jpeg', '<p>gambar no 82</p>\r\n', '#####<p>20</p>\r\n', '#####<p>33</p>\r\n', '#####<p>24</p>\r\n', '#####<p>27</p>\r\n', '#####<p>30</p>\r\n', 'E', '2021-03-20 12:55:22', 0, 0),
(407, 2, 1, 1, 'gambar_soal_407.jpg', 'image/jpeg', '<p>gambar no 83</p>\r\n', '#####<p>4 - 5 - 1 - 2 -3</p>\r\n', '#####<p>4 - 1 - 2 - 5 - 3</p>\r\n', '#####<p>4 - 2 - 5 -1 - 3</p>\r\n', '#####<p>4 - 2 - 1 - 5 - 3</p>\r\n', '#####<p>4 - 3- 2- 1 - 3</p>\r\n', 'D', '2021-03-20 12:55:22', 0, 0),
(408, 2, 1, 1, 'gambar_soal_408.jpg', 'image/jpeg', '<p>gambar 84</p>\r\n', '#####<p>1 -2 - 3 - 4 - 5</p>\r\n', '#####<p>3 - 5 -4 - 2 - 1</p>\r\n', '#####<p>3 - 1 - 4 - 5 - 2</p>\r\n', '#####<p>4 - 5 -3 - 2 - 1</p>\r\n', '#####<p>4 - 3 - 2 - 5 - 1</p>\r\n', 'E', '2021-03-20 12:55:22', 0, 0),
(409, 2, 1, 1, 'gambar_soal_409.jpg', 'image/jpeg', '<p>gambar 85</p>\r\n', '#####<p>2 - 1- 3 - 5 - 4</p>\r\n', '#####<p>2 - 5 - 3 - 1 - 4</p>\r\n', '#####<p>2 - 3 - 1 - 5 - 4</p>\r\n', '#####<p>2 - 5 - 1 - 3 -4</p>\r\n', '#####<p>2 - 3 - 5 - 1 - 4</p>\r\n', 'B', '2021-03-20 12:55:22', 0, 0),
(410, 2, 1, 1, '', '', 'Tahun ini sebuah restoran yang sedang berkembang mempekerjakan ernpat kali jumlah karyawan dari tahun lalu. Tahun lalu jumlah karyawan adalah 75 orang. Saat ini karyawan  yang mengundurkan diri sebanyak 3 orang dan karyawan baru berjumlah tiga kali jumlah karyawan yang mengundurkan diri. Berapakah jurniah karyawan di restoran saat ini', '#####134', '#####153', '#####324', '#####300', '#####306', 'E', '2021-03-20 12:55:22', 0, 0),
(411, 2, 1, 1, '', '', 'Budi memitiki uang Rp. 32.000,- lebih banyak dari Sintha, sementara Anto merniik uang yang  jumlahnya dua kali uang Shinta, sedangkan uang Jono hanya seperempat uang Shinta, dan uang Shinta sendiri adalah Rp. 84.000,-. Berapa uang mereka ber empat jika digabungkan ? ', '#####Rp. 410.000,-  ', '#####. Rp. 389.000,-', '##### Rp. 370.000,-', '#####Rp. 360.000,-     ', '##### Rp. 320.000,-', 'B', '2021-03-20 12:55:22', 0, 0),
(412, 2, 1, 1, '', '', 'Pak Reza memiliki penghasilan Rp.4.700.000,- / bulan, dia berkomitmen untuk menyisihkan 15% setiap bulannya untuk tabungan kuliah anaknya nanti. Biaya listrik per bulan yang harus dibayar sebanyak Rp.350.000,- dan uang saku anaknya per minggu Rp.70.000,-. Berapa sisa penghasilan Pak Reza yang akan digunakan untuk keperluan rurnah tangga lainnya? ', '##### Rp. 3.435.000,-      ', '#####Rp. 1.265.000,-', '#####Rp. 3.365.000,-', '##### Rp. 4.153.000,', '##### Rp. 1.335.000,-', 'C', '2021-03-20 12:55:22', 0, 0),
(413, 2, 1, 1, '', '', '82 x 13 : ( 50 x 4 - ( 119 + 77))', '#####148', '#####14.56', '#####-25.36', '#####125', '#####208', 'E', '2021-03-20 12:55:22', 0, 0),
(414, 2, 1, 1, '', '', '0,961 /  0,24  x  5  + 0,25 x 500 ', '#####1125', '#####2125', '#####145', '#####155', '#####165', 'C', '2021-03-20 12:55:22', 0, 0),
(415, 2, 1, 1, '', '', ' 4 / 8 x  121 / 16 ', '#####6', '#####. 3 / 8  ', '#####. 3 / 16 ', '#####. 1    1/4        ', '#####. 96/16', 'B', '2021-03-20 12:55:22', 0, 0),
(416, 2, 1, 1, '', '', 'Kontrontasi = ', '#####Bertentangan ', '##### Perdamaian ', '#####Tawar menawar', '#####Pertikaian     ', '#####Diskusi', 'D', '2021-03-20 12:55:22', 0, 0),
(417, 2, 1, 1, '', '', 'Prestie = ', '#####Takaran ', '#####Martabat     ', '##### Pas pasan', '#####Harga jual        ', '##### Hasil kerja', 'B', '2021-03-20 12:55:22', 0, 0),
(418, 2, 1, 1, '', '', ' Hipotenusa =', '#####Kesimpulan sementara', '#####Datar', '#####Sisi miring', '#####Diagnosa ', '#####Sudut', 'C', '2021-03-20 12:55:22', 0, 0),
(419, 2, 1, 1, 'gambar_soal_419.jpg', 'image/jpeg', '<p>gambar 95</p>\r\n', 'gja_419.jpg#####<p>A</p>\r\n', 'gjb_419.jpg#####<p>B</p>\r\n', 'gjc_419.jpg#####<p>C</p>\r\n', 'gjd_419.jpg#####<p>D</p>\r\n', 'gje_419.jpg#####<p>E</p>\r\n', 'D', '2021-03-20 12:55:22', 0, 0),
(420, 2, 1, 1, 'gambar_soal_420.jpg', 'image/jpeg', '<p>gambar no 96</p>\r\n', 'gja_420.jpg#####<p>A</p>\r\n', 'gjb_420.jpg#####<p>B</p>\r\n', 'gjc_420.jpg#####<p>C</p>\r\n', 'gjd_420.jpg#####<p>D</p>\r\n', 'gje_420.jpg#####<p>E</p>\r\n', 'B', '2021-03-20 12:55:22', 0, 0),
(421, 2, 1, 1, 'gambar_soal_421.jpg', 'image/jpeg', '<p>gambar no 97</p>\r\n', 'gja_421.jpg#####<p>A</p>\r\n', 'gjb_421.jpg#####<p>B</p>\r\n', 'gjc_421.jpg#####<p>C</p>\r\n', 'gjd_421.jpg#####<p>D</p>\r\n', 'gje_421.jpg#####<p>E</p>\r\n', 'A', '2021-03-20 12:55:22', 0, 0),
(422, 2, 1, 1, '', '', 'H    J4   I   K8   M     L12', '#####M     N16   O', '#####K    M22   L', '#####O    N 16    M', '#####P     O18    N', '#####N   P16     O', 'E', '2021-03-20 12:55:22', 0, 0),
(423, 2, 1, 1, '', '', 'SR7   QP17    NM27', '#####JK27     IH37', '#####ML37    KJ47', '#####KJ47    IH37', '#####JI37     ED47', '#####JI37    FE47', 'D', '2021-03-20 12:55:22', 0, 0),
(424, 2, 1, 1, '', '', ' J63M3     N50Q5         R36U7', '#####V21Z9  ', '#####Y9Z20', '#####V21Y9', '##### W17X9           ', '#####X23R11', 'C', '2021-03-20 12:55:22', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `students`
--

CREATE TABLE `students` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `email` varchar(250) NOT NULL,
  `phone` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `students`
--

INSERT INTO `students` (`id`, `name`, `email`, `phone`) VALUES
(1, 'Kailash Singh', 'ks623039@gmail.com', '8376786284'),
(2, 'Amit Singh', 'amit@gmail.com', '8767327272'),
(3, 'Kailash Sharma', 'kailashsharma@gmail.com', '8376868378'),
(4, 'Siddarth Singh', 'sid@gmail.com', '9278297928'),
(5, 'Fari Singh', 'fari@gmail.com', '9287973287'),
(6, 'Avdhesh Singh', 'avdheshsingh@gmail.com', '8926836988'),
(7, 'Lokesh Singh', 'lokesh@gmail.com', '9328797389'),
(8, 'Lakhan Verma', 'lakhan@gmail.com', '9289728939'),
(9, 'Manish Singh', 'manish@gmail.com', '8973298793'),
(10, 'Lovely Singh', 'lovelysingh@gmail.com', '9328987393'),
(11, 'Vikram Rana', 'rana@gmail.com', '9821797983'),
(12, 'Siddarth Verma', 'siddarthverma@gmail.com', '8732683233');

-- --------------------------------------------------------

--
-- Table structure for table `tr_guru_mapel`
--

CREATE TABLE `tr_guru_mapel` (
  `id` int(6) NOT NULL,
  `id_guru` int(6) NOT NULL,
  `id_mapel` int(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tr_guru_mapel`
--

INSERT INTO `tr_guru_mapel` (`id`, `id_guru`, `id_mapel`) VALUES
(3, 6, 1),
(4, 6, 2),
(5, 6, 3),
(6, 6, 4),
(8, 7, 4),
(9, 2, 1),
(10, 2, 2),
(12, 4, 1),
(13, 4, 2),
(14, 4, 3),
(15, 4, 4),
(16, 5, 1),
(17, 5, 2),
(18, 5, 3),
(19, 5, 4),
(20, 7, 1),
(21, 7, 2),
(22, 7, 3);

-- --------------------------------------------------------

--
-- Table structure for table `tr_guru_tes`
--

CREATE TABLE `tr_guru_tes` (
  `id` int(6) NOT NULL,
  `id_guru` int(6) NOT NULL,
  `id_mapel` int(6) NOT NULL,
  `nama_ujian` varchar(200) NOT NULL,
  `jumlah_soal` int(6) NOT NULL,
  `waktu` int(6) NOT NULL,
  `jenis` enum('acak','set') NOT NULL,
  `detil_jenis` varchar(500) NOT NULL,
  `tgl_mulai` datetime NOT NULL,
  `terlambat` int(3) NOT NULL,
  `token` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tr_ikut_kecermatan`
--

CREATE TABLE `tr_ikut_kecermatan` (
  `id` int(11) NOT NULL,
  `id_kolom` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `jml_benar` int(11) NOT NULL,
  `nilai` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tr_ikut_ujian`
--

CREATE TABLE `tr_ikut_ujian` (
  `id` int(6) NOT NULL,
  `id_tes` int(6) NOT NULL,
  `id_user` int(6) NOT NULL,
  `list_soal` longtext NOT NULL,
  `list_jawaban` longtext NOT NULL,
  `jml_benar` int(6) NOT NULL,
  `nilai` decimal(10,2) NOT NULL,
  `nilai_bobot` decimal(10,2) NOT NULL,
  `tgl_mulai` datetime NOT NULL,
  `tgl_selesai` datetime NOT NULL,
  `status` enum('Y','N') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tr_kecermatan`
--

CREATE TABLE `tr_kecermatan` (
  `id` int(11) NOT NULL,
  `id_kecermatan` int(11) NOT NULL,
  `id_kolom` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `jawaban_kecermatan` longtext
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `added_student`
--
ALTER TABLE `added_student`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `m_admin`
--
ALTER TABLE `m_admin`
  ADD PRIMARY KEY (`id`),
  ADD KEY `kon_id` (`kon_id`);

--
-- Indexes for table `m_guru`
--
ALTER TABLE `m_guru`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `m_kecermatan`
--
ALTER TABLE `m_kecermatan`
  ADD PRIMARY KEY (`id_kecermatan`);

--
-- Indexes for table `m_kolom`
--
ALTER TABLE `m_kolom`
  ADD PRIMARY KEY (`id_kolom`);

--
-- Indexes for table `m_mapel`
--
ALTER TABLE `m_mapel`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `m_siswa`
--
ALTER TABLE `m_siswa`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `m_soal`
--
ALTER TABLE `m_soal`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_guru` (`id_guru`),
  ADD KEY `id_mapel` (`id_mapel`);

--
-- Indexes for table `students`
--
ALTER TABLE `students`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tr_guru_mapel`
--
ALTER TABLE `tr_guru_mapel`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_guru` (`id_guru`),
  ADD KEY `id_mapel` (`id_mapel`);

--
-- Indexes for table `tr_guru_tes`
--
ALTER TABLE `tr_guru_tes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_guru` (`id_guru`),
  ADD KEY `id_mapel` (`id_mapel`);

--
-- Indexes for table `tr_ikut_kecermatan`
--
ALTER TABLE `tr_ikut_kecermatan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tr_ikut_ujian`
--
ALTER TABLE `tr_ikut_ujian`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_tes` (`id_tes`),
  ADD KEY `id_user` (`id_user`);

--
-- Indexes for table `tr_kecermatan`
--
ALTER TABLE `tr_kecermatan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_kecermatan` (`id_kecermatan`),
  ADD KEY `id_user` (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `added_student`
--
ALTER TABLE `added_student`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `m_admin`
--
ALTER TABLE `m_admin`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `m_guru`
--
ALTER TABLE `m_guru`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `m_kecermatan`
--
ALTER TABLE `m_kecermatan`
  MODIFY `id_kecermatan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `m_kolom`
--
ALTER TABLE `m_kolom`
  MODIFY `id_kolom` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `m_mapel`
--
ALTER TABLE `m_mapel`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `m_siswa`
--
ALTER TABLE `m_siswa`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `m_soal`
--
ALTER TABLE `m_soal`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=425;

--
-- AUTO_INCREMENT for table `students`
--
ALTER TABLE `students`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `tr_guru_mapel`
--
ALTER TABLE `tr_guru_mapel`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `tr_guru_tes`
--
ALTER TABLE `tr_guru_tes`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tr_ikut_kecermatan`
--
ALTER TABLE `tr_ikut_kecermatan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tr_ikut_ujian`
--
ALTER TABLE `tr_ikut_ujian`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tr_kecermatan`
--
ALTER TABLE `tr_kecermatan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
